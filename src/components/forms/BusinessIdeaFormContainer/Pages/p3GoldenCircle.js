import React from 'react';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import Dropzone from '../../../elements/Dropzone';
import { Grid } from '@material-ui/core';

export default class Component extends React.Component {
  render() {
    const {
      errors,
      touched,
      setFieldValue,
      setFieldTouched,
      handleAutosave,
      onFilesAdded,
      onFilesRemoved,
      values,
    } = this.props;

    return (
      <React.Fragment>
        <h3>Canvassing of Product - Golden Circle</h3>{' '}
        <p className="sub-text">This page identify the explanation of product canvassing. </p>
        <Grid container style={{ marginBottom: '1rem' }}>
          <Grid item lg={6} md={6} xs={12}>
            <Dropzone
              dataCy="golden-circle-image"
              error={errors.goldenCircleUrl}
              handleTouch={setFieldTouched}
              image={values.goldenCircleUrl}
              initialValues={{
                bodyText: 'Click or drag image here (format jpg/png)',
                captionText: '30 MB Maximum',
              }}
              isFailed={values.goldenCircle.isFailed}
              isUploaded={values.goldenCircle.isUploaded}
              label="Golden Circle of Product"
              name="goldenCircleUrl"
              onFilesAdded={(files) =>
                onFilesAdded(files, 'goldenCircleUrl', setFieldValue, values)
              }
              onFilesRemoved={(files) =>
                onFilesRemoved(files, 'goldenCircleUrl', setFieldValue, values)
              }
              progress={values.goldenCircle.progress}
              required
              style={{ width: '26rem' }}
              touched={touched.goldenCircleUrl}
            />
          </Grid>
        </Grid>
        <Field
          name="goldenCircleWhy"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="golden-circle-why"
              error={errors.goldenCircleWhy}
              label="Why"
              onChange={(e) => {
                setFieldTouched('goldenCircleWhy', true);
                setFieldValue('goldenCircleWhy', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText('product description of why', 700)}
              required
              touched={touched.goldenCircleWhy}
              type="long"
            />
          )}
        />{' '}
        <Field
          name="goldenCircleHow"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="golden-circle-how"
              error={errors.goldenCircleHow}
              label="How"
              onChange={(e) => {
                setFieldTouched('goldenCircleHow', true);
                setFieldValue('goldenCircleHow', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText('product description of how', 700)}
              required
              touched={touched.goldenCircleHow}
              type="long"
            />
          )}
        />{' '}
        <Field
          name="goldenCircleWhat"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="golden-circle-what"
              error={errors.goldenCircleWhat}
              label="What"
              onChange={(e) => {
                setFieldTouched('goldenCircleWhat', true);
                setFieldValue('goldenCircleWhat', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText('product description of what', 700)}
              required
              touched={touched.goldenCircleWhat}
              type="long"
            />
          )}
        />{' '}
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
