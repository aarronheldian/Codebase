import React from 'react';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import Dropzone from '../../../elements/Dropzone';
import { Grid } from '@material-ui/core';

export default class Component extends React.Component {
  render() {
    const {
      errors,
      touched,
      setFieldValue,
      setFieldTouched,
      handleAutosave,
      onFilesAdded,
      onFilesRemoved,
      values,
    } = this.props;

    return (
      <React.Fragment>
        <h3>Canvassing of Product - Value Proposition Canvas</h3>{' '}
        <p className="sub-text">This page identify the explanation of product canvassing. </p>
        <Grid container style={{ marginBottom: '1rem' }}>
          <Grid item lg={6} md={6} xs={12}>
            <Dropzone
              dataCy="value-prop-image"
              error={errors.valuePropCustomerUrl}
              handleTouch={setFieldTouched}
              image={values.valuePropCustomerUrl}
              initialValues={{
                bodyText: 'Click or drag image here (format jpg/png)',
                captionText: '30 MB Maximum',
              }}
              isFailed={values.valuePropCustomer.isFailed}
              isUploaded={values.valuePropCustomer.isUploaded}
              label="Value Proposition Canvas of Product"
              name="valuePropCustomerUrl"
              onFilesAdded={(files) =>
                onFilesAdded(files, 'valuePropCustomerUrl', setFieldValue, values)
              }
              onFilesRemoved={(files) =>
                onFilesRemoved(files, 'valuePropCustomerUrl', setFieldValue, values)
              }
              progress={values.valuePropCustomer.progress}
              required
              style={{ width: '26rem' }}
              touched={touched.valuePropCustomerUrl}
            />
          </Grid>
        </Grid>
        <Field
          name="valuePropCustomerJobs"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="value-prop-jobs"
              error={errors.valuePropCustomerJobs}
              label="Customer's Job"
              onChange={(e) => {
                setFieldTouched('valuePropCustomerJobs', true);
                setFieldValue('valuePropCustomerJobs', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`customer's job of product`, 700)}
              required
              touched={touched.valuePropCustomerJobs}
              type="long"
            />
          )}
        />{' '}
        <Field
          name="valuePropCustomerPain"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="value-prop-pain"
              error={errors.valuePropCustomerPain}
              label="Customer's Pain"
              onChange={(e) => {
                setFieldTouched('valuePropCustomerPain', true);
                setFieldValue('valuePropCustomerPain', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`customer's pain of product`, 700)}
              required
              touched={touched.valuePropCustomerPain}
              type="long"
            />
          )}
        />{' '}
        <Field
          name="valuePropCustomerGain"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="value-prop-gain"
              error={errors.valuePropCustomerGain}
              label="Customer's Gain"
              onChange={(e) => {
                setFieldTouched('valuePropCustomerGain', true);
                setFieldValue('valuePropCustomerGain', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`customer's gain of product`, 700)}
              required
              touched={touched.valuePropCustomerGain}
              type="long"
            />
          )}
        />{' '}
        <Field
          name="valuePropCustomerProductService"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="value-prop-prod-services"
              error={errors.valuePropCustomerProductService}
              label="Products & Services"
              onChange={(e) => {
                setFieldTouched('valuePropCustomerProductService', true);
                setFieldValue('valuePropCustomerProductService', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText('product & services of product', 700)}
              required
              touched={touched.valuePropCustomerProductService}
              type="long"
            />
          )}
        />{' '}
        <Field
          name="valuePropCustomerPainRelievers"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="value-prop-pain-relievers"
              error={errors.valuePropCustomerPainRelievers}
              label="Pain Relievers"
              onChange={(e) => {
                setFieldTouched('valuePropCustomerPainRelievers', true);
                setFieldValue('valuePropCustomerPainRelievers', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText('pain relievers of product', 700)}
              required
              touched={touched.valuePropCustomerPainRelievers}
              type="long"
            />
          )}
        />{' '}
        <Field
          name="valuePropCustomerGainCreators"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="value-prop-pain-gain-creators"
              error={errors.valuePropCustomerGainCreators}
              label="Gain Creators"
              onChange={(e) => {
                setFieldTouched('valuePropCustomerGainCreators', true);
                setFieldValue('valuePropCustomerGainCreators', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText('gain creators of product', 700)}
              required
              touched={touched.valuePropCustomerGainCreators}
              type="long"
            />
          )}
        />{' '}
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
