import { errorHandler } from '../../../constants/copywriting';

import * as Yup from 'yup';

export default Yup.object().shape({
  // === page 1
  feedbackGeneral: Yup.string().max(700, errorHandler.maxLength(700)),

  // === page 2
  feedbackBackground: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackDesc: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackTelkomBenefit: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackCustomerBenefit: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackValueCapture: Yup.string().max(700, errorHandler.maxLength(700)),

  // === page 3
  feedbackGoldenCircle: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackValuePropCustomer: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackAdditionalCanvas: Yup.string().max(700, errorHandler.maxLength(700)),

  // === page 4
  feedbackExecDate: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackOmtm: Yup.string().max(700, errorHandler.maxLength(700)),

  // === page 5
  feedbackSquad: Yup.string().max(700, errorHandler.maxLength(700)),
  feedbackBudget: Yup.string().max(700, errorHandler.maxLength(700)),

  // === page summary
  feedbackSummary: Yup.string()
    .required(errorHandler.required('Overview of Evaluations'))
    .max(700, errorHandler.maxLength(700)),
  decision: Yup.string().required(errorHandler.required('Action')),

  // === page recommendation
  pitchingDate: Yup.string().when('decision', {
    is: 'Accept',
    then: Yup.string().required(errorHandler.required('Pitching Schedule')),
  }),
  pitchingTimeStart: Yup.string().when('decision', {
    is: 'Accept',
    then: Yup.string()
      .required(errorHandler.required('Start'))
      .min(5, errorHandler.required('Finish')),
  }),
  pitchingTimeEnd: Yup.string().when('decision', {
    is: 'Accept',
    then: Yup.string()
      .required(errorHandler.required('Finish'))
      .min(5, errorHandler.required('Finish')),
  }),
  address: Yup.string().when('decision', {
    is: 'Accept',
    then: Yup.string().required(errorHandler.required('Place')),
  }),
  tribe: Yup.string().when('decision', {
    is: 'Accept',
    then: Yup.string().required(errorHandler.required('Tribe')),
  }),
  otherTribe: Yup.string().when('tribe', {
    is: 'Other',
    then: Yup.string()
      .required(errorHandler.required('Tribe'))
      .max(160, errorHandler.maxLength(160)),
  }),
});
