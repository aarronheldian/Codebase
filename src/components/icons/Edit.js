import React from 'react';
import PropTypes from 'prop-types';

export default function Edit(props) {
  const color = props.color;

  return (
    <svg
      height="1.5rem"
      style={{ verticalAlign: 'middle' }}
      viewBox="0 0 16 16"
      width="1.5rem"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M6.581 10.041L5.96 9.42l-.436.436-.155.777.777-.155.436-.436zm.632-.632l4.238-4.237-.623-.623-4.237 4.238.622.622zm3.964-5.774l1.188 1.188a.493.493 0 0 1 0 .697l-5.688 5.688c-.06.06-.135.1-.218.117l-1.505.301a.493.493 0 0 1-.58-.58l.3-1.505a.427.427 0 0 1 .118-.218l5.688-5.688a.493.493 0 0 1 .697 0z"
        fill={color}
        fillRule="nonzero"
      />
    </svg>
  );
}

Edit.defaultProps = {
  color: '#1E2025',
};

Edit.propTypes = {
  color: PropTypes.string,
};
