import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { IMAGES } from '../../../configs';
import styles from './styles.module.scss';

export default class Component extends React.Component {
  _renderSide() {
    return (
      <div className={styles['side']} variant="permanent">
        <img src={IMAGES.LOGO_X} />
      </div>
    );
  }

  render() {
    const { children, title } = this.props;

    return (
      <Grid container direction="column" id="form-layout">
        {this._renderSide()}
        <Grid className={styles['content']}>
          <h2 className={styles['title']}>{title.toUpperCase()}</h2>
          {children}
        </Grid>
      </Grid>
    );
  }
}

Component.defaultProps = {
  children: null,
  title: '',
};

Component.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
};
