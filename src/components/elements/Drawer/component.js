import React from 'react';
import PropTypes from 'prop-types';
import drawerListData from '../../../constants/drawerListData';
import { ICONS, IMAGES } from '../../../configs';
import { clearStorages } from '../../../utils/common';
import styles from './styles.css';

export default class Component extends React.Component {
  _handleLogoutClick() {
    clearStorages();
    location.href = '/';
  }

  _renderMenu(item, idx) {
    const url = item.url.split('?')[0];
    const { pathname } = location;
    const isActive = pathname.toLowerCase().split('/')[1] === url.split('/')[1] ? true : false;
    const src = isActive ? `${item.icon.toUpperCase()}_ACTIVE` : item.icon.toUpperCase();

    if (isActive) {
      return (
        <section className={styles['menu-active']} key={idx}>
          <img className={styles.icon} src={ICONS[src]} title={item.name} />
          <h5 className={styles['menu-item-active']}>{item.name}</h5>
        </section>
      );
    } else {
      return (
        <a className={styles.url} href={item.url} key={idx}>
          <section className={styles.menu}>
            <img className={styles.icon} src={ICONS[src]} title={item.name} />
            <h5 className={styles['menu-item']}>{item.name}</h5>
          </section>
        </a>
      );
    }
  }

  _renderLogout() {
    return (
      <section className={styles['url-logout']}>
        <a className={styles.url} id="logout-button" onClick={this._handleLogoutClick}>
          <section className={styles.menu}>
            <img
              className={`${styles.icon} ${styles['icon-logout']}`}
              src={ICONS.LOGOUT}
              title="logout"
            />
            <h6 className={styles['menu-logout']}>Sign Out</h6>
          </section>
        </a>
      </section>
    );
  }

  render() {
    return (
      <main className={styles.drawer}>
        <img className={styles['product-logo']} src={IMAGES.LOGO} />
        <h6 className={styles['product-name']}>Dashboard Performance</h6>

        {drawerListData.map((item, idx) => this._renderMenu(item, idx))}
        {this._renderLogout()}
      </main>
    );
  }
}

Component.defaultProps = {
  match: {},
};

Component.propTypes = {
  match: PropTypes.object,
};
