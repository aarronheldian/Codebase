import React from 'react';
import styles from './styles.module.scss';
import PropTypes from 'prop-types';
import DownloadIcon from '../../icons/Download';
import CloseIcon from '@material-ui/icons/Close';
import RefreshIcon from '@material-ui/icons/Refresh';
import Button from '../Button';
import { Grid, CircularProgress } from '@material-ui/core';
import ErrorMessage from '../ErrorMessage';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hightlight: false,
    };
    this.fileInputRef = React.createRef();
  }

  _handleOnFilesAdded = (e) => {
    if (this.props.disabled) return;
    const files = e.target.files;

    if (files.length > 0) {
      if (this.props.onFilesAdded) {
        const array = this.fileListToArray(files);
        this.props.onFilesAdded(array[0]);
      }
    }
  };

  _handleOnRetryFiles = () => {
    if (this.props.disabled) return;

    this.props.onFilesAdded(this.props.image);
  };

  _handleOnOpenFileDialog = (e) => {
    e.preventDefault();
    const { handleTouch, name } = this.props;
    handleTouch(name, true);
    if (this.props.disabled) return;

    this.fileInputRef.current.value = null;
    this.fileInputRef.current.click();
  };

  fileListToArray(list) {
    const array = [];
    for (let i = 0; i < list.length; i++) {
      array.push(list[i]);
    }
    return array;
  }

  _handleOnDragOver = (e) => {
    e.preventDefault();
    if (this.props.disabled) return;
    this.setState({ hightlight: true });
  };

  _handleOnDragLeave = () => {
    this.setState({ hightlight: false });
  };

  _isPromise(value) {
    return Boolean(value && typeof value.then === 'function');
  }

  _handleOnDrop = (e) => {
    e.preventDefault();
    if (this.props.disabled) return;

    const files = e.dataTransfer.files;
    if (files.length > 0) {
      if (this.props.onFilesAdded) {
        const array = this.fileListToArray(files);
        this.props.onFilesAdded(array[0]);
      }
      this.setState({ hightlight: false });
    }
  };

  _handleOnRemoveFiles = () => {
    this.props.onFilesRemoved(this.props.image);
  };

  _getClassDropzone() {
    const { disabled } = this.props;

    if (disabled) return `${styles['dropzone']} ${styles['disabled']}`;
    else return `${styles['dropzone']}`;
  }

  _getStyle() {
    let { style } = this.props;
    const { hightlight } = this.state;
    const { image } = this.props;

    if (image) {
      style = {
        ...style,
        ...{ display: 'none' },
      };
    }

    if (hightlight) {
      style = {
        ...style,
        ...{ border: 'dashed .125rem #707a89' },
      };
    }

    return style;
  }

  _getClassDisabled = (className) => {
    const { disabled } = this.props;

    if (disabled) return `${className} ${styles['disabled-text']}`;
    else return `${className}`;
  };

  _getImageURL() {
    const { image } = this.props;

    if (typeof image === 'object') {
      return URL.createObjectURL(image);
    }

    return image;
  }

  _renderOptionalLabel(required) {
    if (!required) {
      return <span className={styles['optional']}>(optional)</span>;
    }

    return;
  }

  _getStyleProgress() {
    const { style, progress } = this.props;

    const fmtStyle = style.width ? { width: style.width } : null;

    if (progress >= 100) {
      return {
        ...fmtStyle,
        display: 'none',
      };
    }

    return fmtStyle;
  }

  render() {
    const {
      acceptFormat,
      dataCy,
      disabled,
      error,
      style,
      progress,
      isUploaded,
      isFailed,
      initialValues,
      touched,
      image,
      label,
      required,
    } = this.props;

    return (
      <>
        <Grid
          align="end"
          className={styles['label']}
          container
          justify="space-between"
          style={style}
        >
          {label !== '' ? <p>{label}</p> : <p style={{ color: '#00000000' }}>.</p>}
          {this._renderOptionalLabel(required)}
        </Grid>
        {image && (
          <div className={styles['image-container']} style={style}>
            <img alt="upload" src={this._getImageURL()} />
            {isUploaded || isFailed ? (
              <div className={styles['topright']}>
                {!disabled && (
                  <Button
                    circle
                    id="close"
                    onClick={this._handleOnRemoveFiles}
                    style={{ margin: 0 }}
                  >
                    <CloseIcon style={{ marginLeft: '-.1875rem', marginTop: '.1875rem' }} />
                  </Button>
                )}
              </div>
            ) : (
              <Grid
                alignItems="center"
                className={styles['progress-container']}
                container
                direction="row"
                justify="center"
              >
                <Grid item>
                  <div className={styles['progress-wrapper']}>
                    <CircularProgress
                      className={styles['progress']}
                      value={progress}
                      variant="static"
                    />
                  </div>
                </Grid>
              </Grid>
            )}
          </div>
        )}
        <div
          className={this._getClassDropzone()}
          id="dropzone"
          onClick={this._handleOnOpenFileDialog}
          onDragLeave={this._handleOnDragLeave}
          onDragOver={this._handleOnDragOver}
          onDrop={this._handleOnDrop}
          style={this._getStyle()}
        >
          <div className={`${styles['content-wrapper']}`}>
            <div className={styles['icon']}>
              <DownloadIcon color={!disabled ? '#707a89' : '#ffffff'} height="2rem" width="2rem" />
            </div>
            <div className={this._getClassDisabled(styles['primary-text'])}>
              {initialValues.bodyText}
            </div>
            <div className={this._getClassDisabled(styles['secondary-text'])}>
              {initialValues.captionText}
            </div>
          </div>
        </div>
        {isFailed && (
          <Grid
            alignItems="center"
            container
            direction="row"
            justify="center"
            style={this._getStyleProgress()}
          >
            <Grid item style={{ marginTop: '1rem' }}>
              <Button circle onClick={this._handleOnRetryFiles} style={{ margin: 0 }}>
                <RefreshIcon style={{ marginLeft: '-.1875rem', marginTop: '.1875rem' }} />
              </Button>
            </Grid>
          </Grid>
        )}
        {touched && error && <ErrorMessage>{error}</ErrorMessage>}
        <input
          accept={acceptFormat}
          className={styles['file-input']}
          data-cy={dataCy}
          disabled={disabled}
          id="dropzone-input"
          onChange={this._handleOnFilesAdded}
          ref={this.fileInputRef}
          type="file"
        />
      </>
    );
  }
}

Component.defaultProps = {
  acceptFormat: 'image/jpeg,image/png',
  dataCy: '',
  disabled: false,
  error: '',
  handleTouch: () => {},
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  initialValues: {
    bodyText: null,
    captionText: null,
  },
  isFailed: null,
  label: '',
  name: '',
  progress: 0,
  required: false,
  style: {},
  touched: false,
};

Component.propTypes = {
  acceptFormat: PropTypes.string,
  dataCy: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  handleTouch: PropTypes.func,
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  initialValues: PropTypes.object,
  isFailed: PropTypes.bool,
  isUploaded: PropTypes.bool.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  progress: PropTypes.number,
  required: PropTypes.bool,
  style: PropTypes.object,
  touched: PropTypes.bool,
};
