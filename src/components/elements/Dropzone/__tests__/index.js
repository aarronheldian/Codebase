import React from 'react';
import { shallow } from 'enzyme';
import Dropzone from '../index';

describe('Dropzone', () => {
  window.URL.createObjectURL = jest.fn();
  afterEach(() => {
    window.URL.createObjectURL.mockReset();
  });

  it('Click Dropzone', () => {
    const actions = {
      onFilesAdded: jest.fn(),
      onFilesRemoved: jest.fn(),
    };

    const wrapper = shallow(<Dropzone {...actions} />);
    wrapper.instance()['fileInputRef'] = {
      current: {
        click: jest.fn(),
      }
    };

    wrapper.find('#dropzone').simulate('click', {
      preventDefault: jest.fn(),
    });

    expect(wrapper).toBeCalled;
  });
  it('Change Input', () => {
    const actions = {
      onFilesAdded: jest.fn(),
      onFilesRemoved: jest.fn(),
    };

    const wrapper = shallow(<Dropzone {...actions} isUploaded={true} />);

    wrapper.find('#dropzone-input').simulate('change', {
      target: {
        files: [
          {
            lastModified: 1500514945000,
            lastModifiedDate: 'Thu Jul 20 2017 09:42:25 GMT+0800 (CST)',
            name: 'dummy.docx',
            size: 223678
          }
        ]
      }
    });

    wrapper.find('#close').simulate('click', {
      preventDefault: jest.fn(),
    });

    expect(wrapper).toBeCalled;
  });
  it('Drag and Drop', () => {
    const actions = {
      onFilesAdded: jest.fn(),
      onFilesRemoved: jest.fn(),
    };

    const wrapper = shallow(<Dropzone {...actions} />);

    let mockEvent = {
      preventDefault: jest.fn(),
      dataTransfer: {
        files: [
          {
            lastModified: 1500514945000,
            lastModifiedDate: 'Thu Jul 20 2017 09:42:25 GMT+0800 (CST)',
            name: 'dummy.docx',
            size: 223678
          }
        ]
      }
    };
    wrapper.find('#dropzone').simulate('drag', mockEvent);
    wrapper.find('#dropzone').simulate('dragover', mockEvent);
    wrapper.find('#dropzone').simulate('dragleave', mockEvent);
    wrapper.find('#dropzone').simulate('drop', mockEvent);

    expect(wrapper).toBeCalled;
  });
  it('Disabled Input', () => {
    const actions = {
      onFilesAdded: jest.fn(),
      onFilesRemoved: jest.fn(),
    };

    const wrapper = shallow(<Dropzone {...actions} disabled />);

    wrapper.find('#dropzone-input').simulate('change', {
      target: {
        files: [
          {
            lastModified: 1500514945000,
            lastModifiedDate: 'Thu Jul 20 2017 09:42:25 GMT+0800 (CST)',
            name: 'dummy.docx',
            size: 223678
          }
        ]
      }
    });

    expect(wrapper).toBeCalled;
  });
  it('Disabled Drag and Drop', () => {
    const actions = {
      onFilesAdded: jest.fn(),
      onFilesRemoved: jest.fn(),
    };

    const wrapper = shallow(<Dropzone {...actions} disabled />);

    let mockEvent = {
      preventDefault: jest.fn(),
      dataTransfer: {
        files: [
          {
            lastModified: 1500514945000,
            lastModifiedDate: 'Thu Jul 20 2017 09:42:25 GMT+0800 (CST)',
            name: 'dummy.docx',
            size: 223678
          }
        ]
      }
    };
    wrapper.find('#dropzone').simulate('drag', mockEvent);
    wrapper.find('#dropzone').simulate('dragover', mockEvent);
    wrapper.find('#dropzone').simulate('dragleave', mockEvent);
    wrapper.find('#dropzone').simulate('drop', mockEvent);

    expect(wrapper).toBeCalled;
  });
  it('Promise Function', () => {
    const actions = {
      onFilesAdded: () => {
        return Promise.resolve({});
      },
      onFilesRemoved: () => {
        return Promise.resolve({});
      },
    };

    const wrapper = shallow(<Dropzone {...actions} />);

    wrapper.find('#dropzone-input').simulate('change', {
      target: {
        files: [
          {
            lastModified: 1500514945000,
            lastModifiedDate: 'Thu Jul 20 2017 09:42:25 GMT+0800 (CST)',
            name: 'dummy.docx',
            size: 223678
          }
        ]
      }
    });

    expect(wrapper).toBeCalled;
  });
});
