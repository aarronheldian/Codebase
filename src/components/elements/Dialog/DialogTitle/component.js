import React from 'react';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle';
import styles from '../styles.css';

export default class Component extends React.Component {
  render() {
    return (
      <DialogTitle {...this.props} className={styles['modal-title']}>
        {this.props.children}
      </DialogTitle>
    );
  }
}

Component.defaultProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};

Component.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};
