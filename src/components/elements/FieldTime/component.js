import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import ErrorMessage from '../ErrorMessage';
import Cleave from 'cleave.js/react';
import { ICONS } from '../../../configs';
import styles from './styles.module.scss';
import Tooltip from '../Tooltip';

export default class FieldText extends React.Component {
  constructor(props) {
    super(props);
    this._getClass = this._getClass.bind(this);
    this._renderOptionalLabel = this._renderOptionalLabel.bind(this);
  }

  _getClass() {
    let className = styles['cdx-input'];
    if (this.props.error && this.props.touched) {
      className = `${className} ${styles['error-input']}`;
    }
    if (this.props.small) {
      className = `${className} ${styles['small']}`;
    }

    return className;
  }

  _getMainClass() {
    let className = styles['cdx-textinput'];
    if (this.props.block) {
      className = `${className} ${styles['block']}`;
    } else if (this.props.long) {
      className = `${className} ${styles['long']}`;
    }

    return className;
  }

  _renderOptionalLabel(required) {
    if (!required) {
      return <span className={styles['optional']}>(optional)</span>;
    }

    return;
  }

  render() {
    const {
      className,
      field,
      info,
      label,
      required,
      touched,
      error,
      style,
      timePattern,
    } = this.props;

    const options = {
      time: {
        time: true,
        timePattern: timePattern,
      },
    };

    return (
      <div className={`${this._getMainClass()} ${className}`} style={style}>
        <Grid alignItems="end" className={styles['label']} container justify="space-between">
          {label !== '' ? (
            <p>
              {label}{' '}
              {info !== '' && (
                <Tooltip placement="bottom-start" title={info}>
                  <img
                    src={ICONS.ICON_TOOLTIP}
                    style={{ height: '1rem', transform: 'translate(0, 0.2rem)' }}
                  />
                </Tooltip>
              )}
            </p>
          ) : (
            <p style={{ color: '#00000000' }}>.</p>
          )}

          {this._renderOptionalLabel(required)}
        </Grid>
        <Cleave
          {...this.props}
          {...field}
          className={this._getClass()}
          options={options.time}
          type="text"
        />

        {touched && error && <ErrorMessage>{error}</ErrorMessage>}
      </div>
    );
  }
}

FieldText.defaultProps = {
  block: false,
  className: '',
  decimal: false,
  error: '',
  field: {},
  info: '',
  label: '',
  limit: 700,
  long: false,
  max: 12,
  required: false,
  small: false,
  style: {},
  timePattern: ['h', 'm'],
  touched: false,
  type: 'text',
  value: '',
};

FieldText.propTypes = {
  block: PropTypes.bool,
  className: PropTypes.string,
  decimal: PropTypes.bool,
  error: PropTypes.string,
  field: PropTypes.object,
  info: PropTypes.string,
  label: PropTypes.string,
  limit: PropTypes.number,
  long: PropTypes.bool,
  max: PropTypes.number,
  required: PropTypes.bool,
  small: PropTypes.bool,
  style: PropTypes.object,
  timePattern: PropTypes.array,
  touched: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};
