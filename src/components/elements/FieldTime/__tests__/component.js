import React from 'react';
import renderer from 'react-test-renderer';
import Component from '../component';
import { Formik, Field } from 'formik';

describe('Field Input', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <Formik>
          <Field component={Component} name="fname" required />
        </Formik>,
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('renders correctly (not required)', () => {
    const tree = renderer.create(<Component long />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders with error', () => {
    const tree = renderer
      .create(<Component error="this is error message" required small touched />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders with block width', () => {
    const tree = renderer.create(<Component block label="Time Input" />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
