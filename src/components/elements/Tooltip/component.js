import { Tooltip } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const CdxTooltip = withStyles({
  tooltip: {
    backgroundColor: 'rgba(66, 90, 112, 0.9);',
    borderRadius: '0.1875rem',
    color: '#ffffff',
    fontSize: '.75rem',
    margin: '0 !important',
    maxWidth: 'none',
    padding: '0.375rem 0.5rem',
    lineHeight: '1.33rem',
    textAlign: 'left',
  },
})(Tooltip);

export default CdxTooltip;
