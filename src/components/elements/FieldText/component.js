import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import ErrorMessage from '../ErrorMessage';
import Cleave from 'cleave.js/react';
import { IMAGES, ICONS } from '../../../configs';
import styles from './styles.module.scss';
import Tooltip from '../Tooltip';

export default class FieldText extends React.Component {
  constructor(props) {
    super(props);
    this._getClass = this._getClass.bind(this);
    this._renderOptionalLabel = this._renderOptionalLabel.bind(this);
    this.state = {
      inputType: this.props.type,
      showPassword: false,
    };
  }

  _getClass() {
    let className = this.props.type === 'long' ? styles['cdx-textarea'] : styles['cdx-input'];
    if (this.props.error && this.props.touched) {
      className = `${className} ${styles['error-input']}`;
    }
    if (this.props.small) {
      className = `${className} ${styles['small']}`;
    }

    return className;
  }

  _getMainClass() {
    let className = styles['cdx-textinput'];
    if (this.props.block) {
      className = `${className} ${styles['block']}`;
    } else if (this.props.long) {
      className = `${className} ${styles['long']}`;
    }

    return className;
  }

  _renderOptionalLabel(required) {
    if (!required) {
      return <span className={styles['optional']}>(optional)</span>;
    }

    return;
  }

  _renderCounter(char) {
    return (
      <p
        className={styles['counter']}
        style={this.props.error && this.props.touched ? { color: '#f55151' } : {}}
      >
        {this.props.limit - char}
      </p>
    ); //limit default 700
  }

  render() {
    const {
      className,
      field,
      decimal,
      info,
      label,
      max,
      required,
      touched,
      error,
      style,
      type,
      value,
    } = this.props;

    function handleShowPasswordClick() {
      this.setState({
        showPassword: !this.state.showPassword,
        inputType: this.state.showPassword ? 'text' : 'password',
      });
    }

    const options = {
      rupiah: {
        prefix: 'Rp ',
        numeral: true,
        numeralPositiveOnly: true,
        noImmediatePrefix: true,
        rawValueTrimPrefix: true,
        numeralIntegerScale: max,
        numeralDecimalScale: 0,
        numeralDecimalMark: ',',
        delimiter: '.',
      },
      number: {
        numeral: true,
        numeralPositiveOnly: true,
        numeralIntegerScale: max,

        numeralDecimalScale: decimal ? 3 : 0,
        numeralDecimalMark: ',',
        delimiter: '.',

        numeralDecimalMarkAlternativeInput: '.',
        completeDecimalsOnBlur: true,
      },
    };

    return (
      <div className={`${this._getMainClass()} ${className}`} style={style}>
        <Grid alignItems="end" className={styles['label']} container justify="space-between">
          {label !== '' ? (
            <p>
              {label}{' '}
              {info !== '' && (
                <Tooltip placement="bottom-start" title={info}>
                  <img
                    src={ICONS.ICON_TOOLTIP}
                    style={{ height: '1rem', transform: 'translate(0, 0.2rem)' }}
                  />
                </Tooltip>
              )}
            </p>
          ) : (
            <p style={{ color: '#00000000' }}>.</p>
          )}

          {this._renderOptionalLabel(required)}
          {type === 'long' && this._renderCounter(value.length)}
        </Grid>
        {type === 'long' ? (
          <textarea {...this.props} {...field} className={this._getClass()} type="text" />
        ) : type === 'rupiah' ? (
          <Cleave
            {...this.props}
            {...field}
            className={this._getClass()}
            options={options.rupiah}
            type="text"
          />
        ) : type === 'number' ? (
          <Cleave
            {...this.props}
            {...field}
            className={this._getClass()}
            options={options.number}
            type="text"
          />
        ) : (
          <input
            {...this.props}
            {...field}
            className={this._getClass()}
            type={this.state.inputType}
          />
        )}
        {type === 'password' && (
          <span className={styles['password-icon']} onClick={handleShowPasswordClick}>
            <img src={!this.state.showPassword ? IMAGES.SHOW_PASSWORD : IMAGES.HIDE_PASSWORD} />
          </span>
        )}

        {touched && error && <ErrorMessage>{error}</ErrorMessage>}
      </div>
    );
  }
}

FieldText.defaultProps = {
  block: false,
  className: '',
  decimal: false,
  error: '',
  field: {},
  info: '',
  label: '',
  limit: 700,
  long: false,
  max: 12,
  required: false,
  small: false,
  style: {},
  touched: false,
  type: 'text',
  value: '',
};

FieldText.propTypes = {
  block: PropTypes.bool,
  className: PropTypes.string,
  decimal: PropTypes.bool,
  error: PropTypes.string,
  field: PropTypes.object,
  info: PropTypes.string,
  label: PropTypes.string,
  limit: PropTypes.number,
  long: PropTypes.bool,
  max: PropTypes.number,
  required: PropTypes.bool,
  small: PropTypes.bool,
  style: PropTypes.object,
  touched: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};
