import React from 'react';
import PropTypes from 'prop-types';
import Popover from '@material-ui/core/Popover';
import DayPicker, { DateUtils } from 'react-day-picker';
import queryString from 'query-string';
import Button from '../../elements/Button';
import { dataPercentageOmtm } from './../../../utils/percentageOmtm';
import { ICONS } from './../../../configs';
import Select from '../../elements/Select';
import { changeDate } from '../../../utils/dateFormat';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      from: (this.props.from === '-') ? null : new Date(this.props.from),
      to: (this.props.to === '-') ? null : new Date(this.props.to),
      enteredTo: (this.props.to === '-') ? null : new Date(this.props.to),
    };
    this._handleClickDatePicker = this._handleClickDatePicker.bind(this);
    this._handleDayMouseEnter = this._handleDayMouseEnter.bind(this);
  }

  _renderSelectingFirstDay(from, to, day) {
    const isBeforeFirstDay = from && DateUtils.isDayBefore(day, from);
    const isRangeSelected = from && to;
    return !from || isBeforeFirstDay || isRangeSelected;
  }

  _handleClickDatePicker(day) {
    const { from, to } = this.state;
    const { onChangeOption } = this.props;

    if (this._renderSelectingFirstDay(from, to, day)) {
      this.setState({
        from: day,
        to: null,
        enteredTo: null,
      });
    } else {
      this.setState({
        to: day,
        enteredTo: day,
        anchorEl: null
      });
      onChangeOption({ isDate: true, from: from, to: day });
    }
  }

  _handleDayMouseEnter(day) {
    const { from, to } = this.state;
    if (!this._renderSelectingFirstDay(from, to, day)) {
      this.setState({
        enteredTo: day
      });
    }
  }

  _handleClickPopOver = event => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };

  _handleClosePopOver = () => {
    this.setState({
      anchorEl: null,
    });
  };

  _renderDatePicker() {
    const { from, enteredTo } = this.state;
    const { classes } = this.props;
    const modifiers = { start: from, end: enteredTo };
    const selectedDays = [from, { from, to: enteredTo }];

    return (
      <div>
        <DayPicker
          className={classes.Selectable}
          modifiers={modifiers}
          numberOfMonths={2}
          onDayClick={this._handleClickDatePicker}
          onDayMouseEnter={this._handleDayMouseEnter}
          selectedDays={selectedDays}
        />
      </div>
    );
  }

  _renderOptionProduct() {
    const { dataProduct, onChangeOption } = this.props;
    const query = queryString.parse(location.search);
    let newProduct = [{ id: undefined, name: 'All' }];    

    if (dataProduct) {
      const value = query.productId ? query.productId : newProduct[0].id;
      dataProduct.map(item => {
        newProduct.push({ id: item.productId, name: item.productName });
      });

      return (
        <div>
          <h4 className={styles['title-description']}>Product Name</h4>
          <Select
            className={styles['select-project-filter']}
            data={newProduct}
            name="productId"
            onChange={onChangeOption}
            value={value}
            width="15.75rem" />
        </div>
      );
    } else return null;
  }

  _renderCalendar() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    const query = queryString.parse(location.search);
    const date = query.filterEndDate ? changeDate(query.filterStartDate) + '~' + changeDate(query.filterEndDate) : 'All';

    return (
      <div>
        <h4 className={styles['title-description']}>Range of Project Start Date</h4>
        <div className={styles['calendar-wrapper']} onClick={this._handleClickPopOver}>
          <h4>{date}</h4>
          <img className={styles['calendar-icon']} src={ICONS.ICON_CALENDAR} />
        </div>
        <Popover
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          className={styles['container-popover']}
          id="simple-popper"
          onClose={this._handleClosePopOver}
          open={open}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          {this._renderDatePicker()}
        </Popover>
      </div>
    );
  }

  _renderOptionOMTM() {
    const { onChangeOption } = this.props;
    const query = queryString.parse(location.search);
    let dataOMTM = [];


    if (dataPercentageOmtm) {
      const value = query.achievement ? query.achievement : dataPercentageOmtm[0].key;
      dataPercentageOmtm.map(item => {
        dataOMTM.push({ id: item.id, name: item.name });
      });

      return (
        <div>
          <h4 className={styles['title-description']}>OMTM Achievement</h4>
          <Select
            className={styles['select-project-filter']}
            data={dataOMTM}
            name="achievement"
            onChange={onChangeOption}
            value={value}
            width="10rem"
          />
        </div>
      );
    } else return null;
  }

  _handleReset = () => {
    this.props.onChangeOption({ name: 'achievement', value: undefined });
    this.props.onChangeOption({ name: 'filterEndDate', value: undefined });
    this.props.onChangeOption({ name: 'filterStartDate', value: undefined });
    this.props.onChangeOption({ name: 'productId', value: undefined });
    this.setState({ enteredTo: null, from: null, to: null });
  }

  render() {
    const query = queryString.parse(location.search);    

    return (
      <div className={styles.container}>
        {this._renderOptionProduct()}
        {this._renderCalendar()}
        {this._renderOptionOMTM()}
        {query.achievement || query.filterEndDate || query.filterStartDate || query.productId ?
          <Button
            className={styles['reset-button']}
            onClick={this._handleReset}
          >Reset</Button> : ''}
      </div>
    );
  }
}

Component.propTypes = {
  classes: PropTypes.object,
  dataProduct: PropTypes.array,
  from: PropTypes.string,
  isLoading: PropTypes.bool,
  omtm: PropTypes.string,
  onChangeOption: PropTypes.func,
  project: PropTypes.string,
  to: PropTypes.string,
};

Component.defaultProps = {
  classes: {},
  dataProduct: [],
  from: '',
  isLoading: true,
  omtm: '',
  onChangeOption: '',
  project: '',
  to: ''
};
