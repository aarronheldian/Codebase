const styles = {
  Selectable: {
    '& .DayPicker-Caption': {
      borderBottom: '0.0625rem solid #43aba4',
      fontSize: '0.875rem',
      paddingBottom: '0.9375rem',
      textAlign: 'center',
    },
    '& .DayPicker-Day--end': {
      '&:focus': {
        outline: 'none'
      },
      backgroundColor: '#289f97 !important',
      borderBottomLeftRadius: '0 !important',
      borderBottomRightRadius: '0.3125rem !important',
      borderTopLeftRadius: '0 !important',
      borderTopRightRadius: '0.3125rem !important',
    },
    '& .DayPicker-Day--end:hover': {
      borderBottomRightRadius: '0.3125rem !important',
      borderTopRightRadius: '0.3125rem !important',
    },
    '& .DayPicker-Day--selected:not(.DayPicker-Day--end):not(.DayPicker-Day--outside):not(.DayPicker-Day--start)': {
      backgroundColor: '#e4f7f6 !important',
      borderRadius: '0 !important',
      color: 'black',
    },
    '& .DayPicker-Day--start': {
      '&:focus': {
        outline: 'none'
      },
      backgroundColor: '#289f97 !important',
      borderBottomLeftRadius: '0.3125rem !important',
      borderBottomRightRadius: '0 !important',
      borderTopLeftRadius: '0.3125rem !important',
      borderTopRightRadius: '0 !important',
    },
    '& .DayPicker-Day--start:hover': {
      borderBottomLeftRadius: '0.3125rem !important',
      borderTopLeftRadius: '0.3125rem !important',
    },
    '& .DayPicker-Day:hover': {
      borderRadius: '0 !important'
    },
    '& .DayPicker-Month': {
      borderCollapse: 'collapse',
      borderSpacing: '0',
      display: 'table',
      margin: 0,
      marginTop: '1rem',
      userSelect: 'none'
    },
    '& .DayPicker-Months': {
      backgroundColor: '#FFFFFF',
      borderRadius: '0.3125rem',
      boxShadow: '0 0.125rem 0.25rem 0 rgba(72, 122, 157, 0.5)',
      fontFamily: 'ubuntu',
    },
    '& .DayPicker-NavButton--next': {
      '&:focus': {
        outline: 'none'
      }
    },
    '& .DayPicker-NavButton--prev': {
      '&:focus': {
        outline: 'none'
      },
      marginRight: '0',
      right: '28rem',
    },
    '& .DayPicker-wrapper': {
      padding: '0',
    },
  }
};

export default styles;
