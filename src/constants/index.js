import actions from './actions';

export const ACTIONS = actions;
export const REGEX_EMAIL_VALIDATION = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const REGEX_EMAIL_MAXCHAR = /^[\w.]{3,30}$/;
export const REGEX_EMAIL_CHARACTER = /^[\w.]+$/;
