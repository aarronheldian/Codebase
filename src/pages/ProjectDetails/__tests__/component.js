import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Component from '../component';

jest.mock('../../../utils/text');
jest.mock('../../../utils/dateFormat');

let newQuery = '';

describe('Component component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      actions: {
        fetchData: jest.fn(),
      },
      data: {
        details: null,
        talent: null,
        achievement: null,
      },
      isLoading: {
        details: true,
        talent: true,
        achievement: true,
      },
      location: {
        search: 'test',
      },
      match: {
        params: {
          id: '1',
        }
      }
    };
    newQuery = { search: '', pathname: '' };

    Object.defineProperty(window, 'location', {
      writable: true,
      value: newQuery,
    });
  });

  it('renders correctly when loading', () => {
    newQuery = { pathname: 'product/details' };

    Object.defineProperty(window, 'location', {
      writable: true,
      value: newQuery,
    });
    props.isLoading.details = true;
    
    const tree = renderer
      .create(<Component {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly when loading with any query', () => {
    newQuery = { search: '?page=2', pathname: 'product/details' };

    Object.defineProperty(window, 'location', {
      writable: true,
      value: newQuery,
    });
    
    props.isLoading.achievement = true;
    const tree = renderer
      .create(<Component {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('load actions if change locatin url', () => {
    const wrapper = shallow(<Component  {...props} />);
    wrapper.setProps({ location: { search: 'url', pathname: 'product/details' } });
    expect(wrapper.instance().props.actions.fetchData).toBeCalled();
  });
});
