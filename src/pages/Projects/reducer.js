import { ACTIONS } from '../../constants';

const initialState = {
  isLoading: {
    product: false,
    list: false,
  },
  data: {
    product: null,
    list: null,
  },
  meta: null
};

export default function reducer(state = initialState, action) {
  const { type, data, meta, name } = action;
  switch (type) {
    case ACTIONS.LOADING:
      return {
        ...state,
        isLoading: {
          ...state.isLoading,
          [name]: true,
        },
      };
    case ACTIONS.LIST_PROJECT_FETCHED:
      return {
        ...state,
        isLoading: {
          ...state.isLoading,
          [name]: false,
        },
        data: {
          ...state.data,
          [name]: data,
        },
      };
    case ACTIONS.META_PROJECT:
      return {
        ...state,
        meta: meta,
      };
    default:
      return state;
  }
}
