import * as actions from '../action';
import fetch from '../../../utils/fetch';

jest.mock('../../../utils/fetch');
let dispatch;
let newQuery = '';

describe('Actions', () => {
  beforeEach(() => {
    dispatch = jest.fn();
  });

  it('calls dispatch when fetchData success with Query', async () => {
    const mockFn = () => new Promise((resolve) => {
      resolve({
        data: [
          [
            {
              status: 1,
              updated_at: new Date(),
            },
            {
              status: 2,
              updated_at: new Date(),
            }
          ]
        ],
        status: 'success',
        code: 200,
        meta: {
          current_page: 1,
          total_pages: 1,
          total_data: 6,
          per_page: 10
        }
      });
    });
    newQuery = { search: '' };

    Object.defineProperty(window, 'location', {
      writable: true,
      value: { search: newQuery.search }
    });

    fetch.mockImplementation(mockFn);
    expect.assertions(1);
    await actions.getListBusinessIdea()(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });
});
