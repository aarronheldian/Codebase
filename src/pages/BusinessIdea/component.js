import React from 'react';
import PropTypes from 'prop-types';
import BusinessIdeaList from '../../components/fragments/BusinessIdeaTable';
import styles from './styles.css';
import services from '../../configs/services';
import socketIOClient from 'socket.io-client';
import PageBase from '../../components/layouts/PageBase';
import Button from '../../components/elements/Button';
import Fetching from '../../components/elements/Fetching';
import { ROUTES } from '../../configs';
import { Grid } from '@material-ui/core';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._handleOnChangeUrl = this._handleOnChangeUrl.bind(this);
    this._handleOnAddItem = this._handleOnAddItem.bind(this);
    this._handleOnRefreshPage = this._handleOnRefreshPage.bind(this);
    this._handleOnDeleteItem = this._handleOnDeleteItem.bind(this);
    this._handleOnDownloadItem = this._handleOnDownloadItem.bind(this);
    this._handleOnEditItem = this._handleOnEditItem.bind(this);
  }

  componentDidMount() {
    this.props.actions.getListBusinessIdea();
    this.connectSocketIO();
  }

  connectSocketIO() {
    const socket = socketIOClient(services.BASE_URL_PMS_SOCKET);
    // On Refresh
    socket.on(`refresh:${this.props.nik}`, () => {
      this.props.actions.getListBusinessIdea();
    });
  }

  _handleOnChangeUrl(query, page) {
    this.props.actions.setQuery({ name: 'page', value: page });
    this.props.actions.getListBusinessIdea();
  }

  _handleOnAddItem() {
    this.props.actions.postBusinessIdea(this.props.nik).then((res) => {
      window.open(`${ROUTES.BUSINESS_IDEA_FORM(res.id)}`, '_blank');
    });
  }

  _handleOnRefreshPage() {
    this.props.actions.getListBusinessIdea();
  }

  _handleOnDeleteItem(id) {
    return new Promise((resolve, reject) => {
      this.props.actions
        .deleteBusinessIdea(id)
        .then((res) => {
          this.props.openSnackbar({
            isOpen: true,
            variant: 'success',
            message: 'Your business idea proposal has been successfully deleted!',
          });
          resolve(res);
          this._handleOnRefreshPage();
        })
        .catch((err) => {
          this.props.openSnackbar({
            isOpen: true,
            variant: 'error',
            message: 'Something went wrong. Your business idea proposal can not be deleted.',
          });
          reject(err);
        });
    });
  }

  _handleOnDownloadItem(id) {
    this.props.actions.downloadBusinessIdea(id).catch(() => {
      this.props.openSnackbar({
        isOpen: true,
        variant: 'error',
        message: 'Something went wrong. Your business idea proposal can not be downloaded.',
      });
    });
  }

  _handleOnEditItem(id) {
    window.open(`${ROUTES.BUSINESS_IDEA_FORM(id)}`, '_blank');
  }

  render() {
    const { isLoading, meta, data } = this.props;

    const _renderShowingResult = () => {
      let resultMax = meta.page * 10;

      if (this.props.meta.totalData < meta.page * 10) {
        resultMax = this.props.meta.totalData;
      }

      return `${(meta.page - 1) * 10 + 1} - ${resultMax}`;
    };

    return (
      <PageBase>
        {isLoading ? (
          <Fetching />
        ) : (
          <section>
            <header className={styles.header}>
              <h3>Business Idea Proposal</h3>
            </header>
            <Grid container direction="row">
              <Grid container justify="flex-start">
                <h5>
                  Showing Results <strong>{_renderShowingResult()}</strong> of{' '}
                  <strong>{meta.totalData}</strong>
                </h5>
              </Grid>
              <Grid container justify="flex-end">
                <Button onClick={this._handleOnAddItem}>Add Proposal</Button>
              </Grid>
            </Grid>
            <BusinessIdeaList
              {...this.props}
              data={data}
              meta={meta}
              onChangeUrl={this._handleOnChangeUrl}
              onDeleteItem={this._handleOnDeleteItem}
              onDownloadItem={this._handleOnDownloadItem}
              onEditItem={this._handleOnEditItem}
            />
          </section>
        )}
      </PageBase>
    );
  }
}

Component.defaultProps = {
  classes: {},
};

Component.propTypes = {
  actions: PropTypes.object.isRequired,
  classes: PropTypes.object,
  data: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  nik: PropTypes.number.isRequired,
  openSnackbar: PropTypes.func.isRequired,
};
