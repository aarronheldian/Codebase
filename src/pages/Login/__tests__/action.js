import crypto from 'crypto';
import * as actions from '../action';
import fetch from '../../../utils/fetch';

jest.mock('crypto');
jest.mock('../../../utils/common');
jest.mock('../../../utils/fetch');

crypto.createCipher = jest.fn(() => ({
  update: jest.fn(),
  final: jest.fn(),
}));

let dispatch;

describe('Actions', () => {
  beforeEach(() => {
    dispatch = jest.fn();
    const mockFn = (param) => new Promise((resolve, reject) => {
      if (param.data && param.data.username === 'test') {
        if (param.data.access_token === 'test') resolve({ data: 'success', accessToken: 'success' });
        else resolve({ data: 'success' });
      } else reject({ data: { message: 'fail' } });
    });

    fetch.mockImplementation(mockFn);
  });

  it('calls dispatch when success', async () => {
    expect.assertions(1);
    await actions.fetchLogin({ username: 'test' })(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });

  it('calls dispatch when error', async () => {
    expect.assertions(1);
    await actions.fetchLogin({})(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });

  it('calls dispatch with access token', async () => {
    expect.assertions(1);
    await actions.fetchLogin({ username: 'test', access_token: 'test' })(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });
});
