import queryString from 'query-string';
import { push } from 'react-router-redux';
import fetch from '../../utils/fetch';
import { ACTIONS } from '../../constants';
import { AUTHORIZATION, SERVICES } from '../../configs';

export function setQuery(item) {
  if (item.isDate) {
    return (dispatch) => {
      const value = queryString.parse(location.search);
      value.startDate = item.from.toISOString();
      value.endDate = item.to.toISOString();
      const urlParser = `?${queryString.stringify(value)}`;
      dispatch(push(urlParser));
    };
  } else {
    return (dispatch) => {
      const value = queryString.parse(location.search);
      value[item.name] = item.value;
      const urlParser = `?${queryString.stringify(value)}`;
      dispatch(push(urlParser));
    };
  }
}

function setUrl(type) {
  const url = {
    product: SERVICES.GET_LIST_PRODUCT_SUMMARY,
    productFilter: SERVICES.GET_LIST_PRODUCT_FILTER,
    tribeFilter: SERVICES.GET_LIST_TRIBE_FILTER,
  };
  return url[type];
}

export function fetchData(type, res) {  
  const query = queryString.parse(location.search);
  query.count = 7;

  return (dispatch) => {
    const options = {
      method: 'get',
      url: setUrl(type),
      params: query,
      headers: {
        Authorization: AUTHORIZATION,
      }
    };

    dispatch(loadingAction(type));

    fetch(options)
      .then(response => {
        dispatch(dataFetchedAction(response.data, type));
        if(response.meta) {
          dispatch(metaFetchedAction(response.meta));
        }
      })
      .catch(() => {
        dispatch(dataFetchedAction(res, type));
        dispatch(metaFetchedAction(null));
      });
  };
}

function dataFetchedAction(data, name) {
  return { type: ACTIONS.LIST_PRODUCT_FETCHED, data, name };
}

function loadingAction(name) {
  return { type: ACTIONS.LOADING, name };
}

function metaFetchedAction(meta) {
  return { type: ACTIONS.META_PRODUCT, meta };
}
