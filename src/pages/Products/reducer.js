import { ACTIONS } from '../../constants';

const initialState = {
  isLoading: {
    product: false,
    productFilter: false,
    tribeFilter: false
  },
  data: {
    product: null,
    productFilter: null,
    tribeFilter: null,
  },
  meta: null
};

export default function reducer(state = initialState, action) {
  const { type, data, meta, name } = action;
  switch (type) {
    case ACTIONS.LOADING:
      return {
        ...state,
        isLoading: {
          ...state.isLoading,
          [name]: true,
        },
      };
    case ACTIONS.LIST_PRODUCT_FETCHED:
      return {
        ...state,
        isLoading: {
          ...state.isLoading,
          [name]: false,
        },
        data: {
          ...state.data,
          [name]: data,
        },
      };
    case ACTIONS.META_PRODUCT:
      return {
        ...state,
        meta: meta,
      };
    default:
      return state;
  }
}
