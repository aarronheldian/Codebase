/* eslint-disable max-lines */
import axios from 'axios';
import { customerList } from '../../utils/dropdownList';
import services from '../../configs/services';
import _ from 'lodash';

function HtmlToPlainText(value) {
  return value.replace(/<[^>]+>/g, '');
}

export async function handleFetchData(id) {
  return new Promise((resolve, reject) => {
    axios
      .get(`${services.GET_TRIBE_ZERO(`/${id}`)}`)
      .then((result) => {
        const data = result.data.data;

        let initState = { id: data.id };

        // === Page 1
        if (data.name) initState = { ...initState, name: data.name };
        if (data.productType) initState = { ...initState, productType: data.productType };
        if (data.productName) initState = { ...initState, productName: data.productName };
        if (data.productLogo)
          initState = {
            ...initState,
            productLogoUrl: services.GET_BUSINESS_IDEA_LOGO(data.productLogo),
            productLogo: {
              ...initState.goldenCircle,
              isUploaded: true,
            },
          };
        if (data.type) initState = { ...initState, type: data.type };
        if (data.area) initState = { ...initState, area: data.area };
        if (data.businessField)
          initState = { ...initState, businessField: data.businessField.array };
        if (data.customer) {
          let findValue = _.find(customerList, function(item) {
            return item.value === result.data.data.customer;
          });

          if (!findValue) {
            initState = {
              ...initState,
              customer: 'Other',
              otherCustomer: data.customer,
            };
          } else {
            initState = {
              ...initState,
              customer: data.customer,
              otherCustomer: '',
            };
          }
        }
        if (data['comment.page1'])
          initState = { ...initState, feedbackGeneral: data['comment.page1'] };

        // === Page 2
        if (data.background)
          initState = {
            ...initState,
            background: data.background,
            backgroundPlain: HtmlToPlainText(data.background),
          };
        if (data['comment.page2_background'])
          initState = { ...initState, feedbackBackground: data['comment.page2_background'] };
        if (data.desc)
          initState = {
            ...initState,
            desc: data.desc,
            descPlain: HtmlToPlainText(data.desc),
          };
        if (data['comment.page2_desc'])
          initState = {
            ...initState,
            feedbackDesc: data['comment.page2_desc'],
          };
        if (data.telkomBenefit)
          initState = {
            ...initState,
            telkomBenefit: data.telkomBenefit,
            telkomBenefitPlain: HtmlToPlainText(data.telkomBenefit),
          };
        if (data['comment.page2_telkom_benefit'])
          initState = {
            ...initState,
            feedbackTelkomBenefit: data['comment.page2_telkom_benefit'],
          };
        if (data.customerBenefit)
          initState = {
            ...initState,
            customerBenefit: data.customerBenefit,
            customerBenefitPlain: HtmlToPlainText(data.customerBenefit),
          };
        if (data['comment.page2_customer_benefit'])
          initState = {
            ...initState,
            feedbackCustomerBenefit: data['comment.page2_customer_benefit'],
          };
        if (data.valueCapture)
          initState = {
            ...initState,
            valueCapture: data.valueCapture,
            valueCapturePlain: HtmlToPlainText(data.valueCapture),
          };
        if (data['comment.page2_value_capture'])
          initState = {
            ...initState,
            feedbackValueCapture: data['comment.page2_value_capture'],
          };

        // === Page 3
        if (data.goldenCircleUrl)
          initState = {
            ...initState,
            goldenCircleUrl: services.GET_BUSINESS_IDEA_CANVAS(data.goldenCircleUrl),
            goldenCircle: {
              ...initState.goldenCircle,
              isUploaded: true,
            },
          };
        if (data.goldenCircleWhy)
          initState = { ...initState, goldenCircleWhy: data.goldenCircleWhy };
        if (data.goldenCircleHow)
          initState = { ...initState, goldenCircleHow: data.goldenCircleHow };
        if (data.goldenCircleWhat)
          initState = { ...initState, goldenCircleWhat: data.goldenCircleWhat };
        if (data['comment.page3'])
          initState = {
            ...initState,
            feedbackGoldenCircle: data['comment.page3'],
          };
        if (data.valuePropCustomerUrl)
          initState = {
            ...initState,
            valuePropCustomerUrl: services.GET_BUSINESS_IDEA_CANVAS(data.valuePropCustomerUrl),
            valuePropCustomer: {
              ...initState.valuePropCustomer,
              isUploaded: true,
            },
          };
        if (data.valuePropCustomerJobs)
          initState = {
            ...initState,
            valuePropCustomerJobs: data.valuePropCustomerJobs,
          };
        if (data.valuePropCustomerPain)
          initState = {
            ...initState,
            valuePropCustomerPain: data.valuePropCustomerPain,
          };
        if (data.valuePropCustomerGain)
          initState = {
            ...initState,
            valuePropCustomerGain: data.valuePropCustomerGain,
          };
        if (data.valuePropCustomerProductService)
          initState = {
            ...initState,
            valuePropCustomerProductService: data.valuePropCustomerProductService,
          };
        if (data.valuePropCustomerPainRelievers)
          initState = {
            ...initState,
            valuePropCustomerPainRelievers: data.valuePropCustomerPainRelievers,
          };
        if (data.valuePropCustomerGainCreators)
          initState = {
            ...initState,
            valuePropCustomerGainCreators: data.valuePropCustomerGainCreators,
          };
        if (data['comment.page4'])
          initState = {
            ...initState,
            feedbackValuePropCustomer: data['comment.page4'],
          };
        if (data.leanCanvasUrl)
          initState = {
            ...initState,
            leanCanvasUrl: services.GET_BUSINESS_IDEA_CANVAS(data.leanCanvasUrl),
            leanCanvas: {
              ...initState.leanCanvas,
              isUploaded: true,
            },
          };
        if (data.leanCanvasDesc)
          initState = {
            ...initState,
            additionalCanvasDesc: data.leanCanvasDesc,
          };
        if (data.businessModelUrl)
          initState = {
            ...initState,
            businessModelUrl: services.GET_BUSINESS_IDEA_CANVAS(data.businessModelUrl),
            businessModel: {
              ...initState.businessModel,
              isUploaded: true,
            },
          };
        if (data.businessModelDesc)
          initState = {
            ...initState,
            additionalCanvasDesc: data.businessModelDesc,
          };

        if (data.additionalCanvasName) {
          initState = {
            ...initState,
            additionalCanvasName: data.additionalCanvasName,
          };

          if (data['comment.page5_lean_canvas'] && data.additionalCanvasName === 'Lean Canvas') {
            initState = {
              ...initState,
              feedbackAdditionalCanvas: data['comment.page5_lean_canvas'],
            };
          } else if (
            data['comment.page5_business_model_canvas'] &&
            data.additionalCanvasName === 'Business Model Canvas'
          ) {
            initState = {
              ...initState,
              feedbackAdditionalCanvas: data['comment.page5_business_model_canvas'],
            };
          }
        }

        // === page 4
        if (data.omtm) initState = { ...initState, omtm: data.omtm };
        if (data.omtmDesc) initState = { ...initState, omtmDesc: data.omtmDesc };
        if (data.omtmTarget)
          initState = {
            ...initState,
            omtmTarget: data.omtmTarget,
            omtmTargetFormatted: data.omtmTarget.replace('.', ','),
          };
        if (data['comment.page6_metric_omtm'])
          initState = {
            ...initState,
            feedbackOmtm: data['comment.page6_metric_omtm'],
          };
        if (data.execStart && data.execEnd)
          initState = {
            ...initState,
            execDate: { startDate: data.execStart, endDate: data.execEnd },
          };
        if (data['comment.page6_time_of_implementation'])
          initState = {
            ...initState,
            feedbackExecDate: data['comment.page6_time_of_implementation'],
          };

        // === page 5

        let talentData = [],
          squadData = [];

        if (data.squadRequest) {
          data.squadRequest.array.map((squad) => {
            talentData = [];

            squad.talent_request.map((talent) => {
              talentData.push({
                jobRole: talent.job_role || '',
                jobLevel: talent.job_level || '',
                amountSubmitted: talent.submitted || '',
                amountExisting: talent.existing || talent.existing === 0 ? talent.existing : '',
                talentExisting: talent.existing_talent || [],
              });
            });

            squadData.push({
              squadName: squad.squad_name || '',
              talentRequest: talentData,
            });
          });

          initState = { ...initState, squadRequest: squadData };
        }
        if (data['comment.page7'])
          initState = {
            ...initState,
            feedbackSquad: data['comment.page7'],
          };

        if (data.costTalent) initState = { ...initState, costTalent: parseInt(data.costTalent) };
        if (data.costEnabler) initState = { ...initState, costEnabler: parseInt(data.costEnabler) };
        if (data.rewardForTalent)
          initState = {
            ...initState,
            rewardForTalent: parseInt(data.rewardForTalent),
          };
        if (data.development) initState = { ...initState, development: parseInt(data.development) };
        if (data.partnership) initState = { ...initState, partnership: parseInt(data.partnership) };
        if (data.operational) initState = { ...initState, operational: parseInt(data.operational) };
        if (data['comment.page8'])
          initState = {
            ...initState,
            feedbackBudget: data['comment.page8'],
          };

        if (data['comment.page9_comment'])
          initState = {
            ...initState,
            feedbackSummary: data['comment.page9_comment'],
          };

        if (data['comment.page9_decision'])
          initState = {
            ...initState,
            decision: data['comment.page9_decision'],
          };

        if (data['comment.page10_pitching_schedule'])
          initState = {
            ...initState,
            pitchingDate: data['comment.page10_pitching_schedule'].date,
            pitchingTimeStart: data['comment.page10_pitching_schedule'].time_start,
            pitchingTimeEnd: data['comment.page10_pitching_schedule'].time_end,
          };

        if (data['comment.page10_tribe'])
          initState = {
            ...initState,
            tribe: data['comment.page10_tribe'],
          };

        if (data['comment.page10_place'])
          initState = {
            ...initState,
            address: data['comment.page10_place'],
          };

        resolve(initState);
      })

      .catch((e) => {
        reject(e);
      });
  });
}

export function handleSendForm(data, isComplete = false) {
  return new Promise(async (resolve, reject) => {
    // submitForm
    let sendData = {};

    if (data.feedbackGeneral && data.feedbackGeneral !== '')
      sendData = { ...sendData, page1: data.feedbackGeneral };

    if (data.feedbackBackground && data.feedbackBackground !== '')
      sendData = { ...sendData, page2_background: data.feedbackBackground };
    if (data.feedbackDesc && data.feedbackDesc !== '')
      sendData = { ...sendData, page2_desc: data.feedbackDesc };
    if (data.feedbackTelkomBenefit && data.feedbackTelkomBenefit !== '')
      sendData = { ...sendData, page2_telkom_benefit: data.feedbackTelkomBenefit };
    if (data.feedbackCustomerBenefit && data.feedbackCustomerBenefit !== '')
      sendData = { ...sendData, page2_customer_benefit: data.feedbackCustomerBenefit };
    if (data.feedbackValueCapture && data.feedbackValueCapture !== '')
      sendData = { ...sendData, page2_value_capture: data.feedbackValueCapture };

    if (data.feedbackGoldenCircle && data.feedbackGoldenCircle !== '')
      sendData = { ...sendData, page3: data.feedbackGoldenCircle };
    if (data.feedbackValuePropCustomer && data.feedbackValuePropCustomer !== '')
      sendData = { ...sendData, page4: data.feedbackValuePropCustomer };
    if (data.feedbackAdditionalCanvas && data.feedbackAdditionalCanvas !== '') {
      if (data.additionalCanvasName === 'Lean Canvas')
        sendData = { ...sendData, page5_lean_canvas: data.feedbackAdditionalCanvas };
      else sendData = { ...sendData, page5_business_model_canvas: data.feedbackAdditionalCanvas };
    }
    if (data.feedbackOmtm && data.feedbackOmtm !== '')
      sendData = { ...sendData, page6_metric_omtm: data.feedbackOmtm };
    if (data.feedbackExecDate && data.feedbackExecDate !== '')
      sendData = { ...sendData, page6_time_of_implementation: data.feedbackExecDate };

    if (data.feedbackSquad && data.feedbackSquad !== '')
      sendData = { ...sendData, page7: data.feedbackSquad };

    if (data.feedbackBudget && data.feedbackBudget !== '')
      sendData = { ...sendData, page8: data.feedbackBudget };

    if (data.feedbackSummary && data.feedbackSummary !== '')
      sendData = { ...sendData, page9_comment: data.feedbackSummary };

    if (data.decision && data.decision !== '')
      sendData = { ...sendData, page9_decision: data.decision };

    if (data.decision === 'Accept') {
      sendData = {
        ...sendData,
        page10_pitching_schedule: {
          date: data.pitchingDate,
          time_start: data.pitchingTimeStart,
          time_end: data.pitchingTimeEnd,
        },
      };
      if (data.tribe && data.tribe !== '') {
        sendData = {
          ...sendData,
          page10_tribe: data.tribe === 'Other' ? data.otherTribe : data.tribe,
        };
        if (data.tribe === 'Other') await addNewTribe(data.otherTribe);
      }
      if (data.address && data.address !== '') {
        sendData = {
          ...sendData,
          page10_place: data.address,
        };
      }
    }

    const status = isComplete ? 'save' : 'draft';

    axios
      .post(services.SAVE_TRIBE_ZERO(data.id, status), sendData)
      .then(() => {
        resolve(true);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

export function handleSendImage(data, progressFunc) {
  return new Promise((resolve, reject) => {
    let formData = new FormData();
    if (data.name !== 'productLogoUrl') formData.append('name', data.name);
    formData.append('image', data.files);

    const uploadUrl =
      data.name === 'productLogoUrl'
        ? services.SAVE_BUSINESS_IDEA_LOGO(data.id)
        : services.SAVE_BUSINESS_IDEA_CANVAS(data.id);

    axios
      .post(uploadUrl, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        onUploadProgress: function(progressEvent) {
          progressFunc(progressEvent);
        }.bind(this),
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        if (err.message === 'Network Error') {
          return resolve(err.message);
        }

        reject(err);
      });
  });
}

export function handleDeleteImage(data) {
  return new Promise((resolve, reject) => {
    const deleteUrl =
      data.name === 'productLogo'
        ? services.DELETE_BUSINESS_IDEA_LOGO(data.id)
        : services.DELETE_BUSINESS_IDEA_CANVAS(data.id);

    axios
      .delete(deleteUrl)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

export function fetchTribeList() {
  return new Promise((resolve) => {
    axios.get(services.GET_LIST_TRIBE).then((res) => {
      let tribeList = [];
      const data = res.data.data;
      data.map((tribe) => {
        tribeList.push({ label: tribe.name, value: tribe.name });
      });
      tribeList.push({ label: 'Other', value: 'Other' });
      resolve(tribeList);
    });
  });
}

function addNewTribe(name) {
  return new Promise((resolve, reject) => {
    const data = { name };
    axios
      .post(services.ADD_TRIBE, data)
      .then((res) => resolve(res))
      .catch((e) => reject(e));
  });
}

export function fetchAdressList() {
  return new Promise((resolve) => {
    axios.get(services.GET_LIST_PITCHING_ADDRESS).then((res) => {
      let addressList = [];
      const data = res.data.data;
      data.map((addr) => {
        addressList.push({
          label: `${addr.name} - ${addr.desc}`,
          value: { id: addr.id, name: addr.name, address: addr.desc },
        });
      });
      resolve(addressList);
    });
  });
}
