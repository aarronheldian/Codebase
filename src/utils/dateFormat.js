export function changeDate(date) {
  const newDate = new Date(date);
  const setDate = (newDate.getDate() < 10) ? `0${newDate.getDate()}` : newDate.getDate();
  const setMonth = (newDate.getMonth()+1 < 10) ? `0${newDate.getMonth()+1}` : newDate.getMonth()+1;

  return `${setDate}/${setMonth}/${newDate.getFullYear()}`;
}
