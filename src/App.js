import React from 'react';
import { Switch, Route } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Snackbar from './components/elements/Snackbar';
import pages from './pages';
import { ROUTES, ICONS } from './configs';
import { getToken } from './utils/common';
import { theme } from './styles';

export default class App extends React.Component {
  constructor() {
    super();

    const [isLoggedIn, shouldRender] = this._checkAuth();

    this.state = {
      isLoggedIn,
      shouldRender,
    };
  }

  _checkAuth() {
    const { pathname } = location;
    const { HOME, LOGIN } = ROUTES;
    const accessToken = getToken();
    const validToken = accessToken;
    const isLoggedIn = pathname === LOGIN();
    let shouldRender = true;

    if (!validToken && pathname !== LOGIN()) {
      location.href = LOGIN();
      shouldRender = false;
    } else if (validToken && pathname === LOGIN()) {
      location.href = HOME();
      shouldRender = false;
    }

    return [isLoggedIn, shouldRender];
  }

  _renderLogin() {
    const { Login } = pages;

    return (
      <main>
        <CssBaseline />
        <Login />
      </main>
    );
  }

  _renderApp() {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Switch>
          <Route component={pages.Products} exact path={ROUTES.HOME()} />
          <Route component={pages.Projects} exact path={ROUTES.PROJECTS()} />
          <Route component={pages.BusinessIdea} exact path={ROUTES.BUSINESS_IDEA()} />
          <Route component={pages.TribeZero} exact path={ROUTES.TRIBE_ZERO()} />
          <Route component={pages.TribeZeroComment} exact path={ROUTES.TRIBE_ZERO_COMMENT(':id')} />
          <Route component={pages.ProjectDetails} exact path={ROUTES.PROJECT_DETAILS(':id')} />
          <Route component={pages.BusinessIdeaForm} exact path={ROUTES.BUSINESS_IDEA_FORM(':id')} />
          <Route component={pages.Error404} />
        </Switch>
      </MuiThemeProvider>
    );
  }

  render() {
    const { isLoggedIn, shouldRender } = this.state;

    if (!shouldRender) {
      return null;
    }
    return (
      <>
        {isLoggedIn ? this._renderLogin() : this._renderApp()}
        <Snackbar />
        <img src={ICONS.SNACKBAR_WARNING} style={{ display: 'none' }} />
      </>
    );
  }
}
